package components_test

import (
	"context"
	"errors"
	"testing"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
	"google.golang.org/grpc/test/bufconn"

	"auth/components"
	pb "auth/grpc/auth"
	"net"

	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
)


type MockStore struct {
	components.DataStore
}

func (s *MockStore) CheckExist(email string) (bool, error) {
	return email == "exist@e.mail", nil
}

func (s *MockStore) CreateAccount(data *pb.AccountData)   (uuid.UUID, error) {
	account := &components.AccountModel{
		Id: uuid.New(),
		Email: data.Email,
		IsActive: false,
	}
	if data.Email == "error@e.mail" {
		return account.Id, errors.New("empty name")
	} else {
		return account.Id, nil
	}
}

func (store *MockStore) ActivateAccount(id uuid.UUID) (error) {
	return nil
}

func (store *MockStore) GetAccountByField(id string, field string) (*components.AccountModel, error) {
	account := &components.AccountModel{Id: uuid.New(), Email: id, IsActive: false}
	account.SetPassword("secret")
	return account, nil
}

func dialer() func(context.Context, string) (net.Conn, error) {

	listener := bufconn.Listen(1024 * 1024)
	server := grpc.NewServer()
	jwt := &components.JWT{Secret: []byte("secret"), AccessExpiration: 5, RefreshExpiration: 5, ActivationExpiration: 5}
	pb.RegisterAuthServiceServer(server, &components.AuthService{Log: log.New(), JWT: jwt, Store: &MockStore{}})
 
	go func() {
		if err := server.Serve(listener); err != nil {
			log.Fatal(err)
		}
	}()
 
	return func(context.Context, string) (net.Conn, error) {
		return listener.Dial()
	}
}


func TestAuth(t *testing.T) {
	ctx := context.Background()
	conn, err := grpc.DialContext(ctx, "", grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithContextDialer(dialer()))
	if err != nil {t.Fatal(err)}
	defer conn.Close()
 
	client := pb.NewAuthServiceClient(conn)

	tests := []struct {
		name    string
		req *pb.AccountData
		errCode codes.Code
	}{
		{
			"test create account",
			&pb.AccountData{Email: "test@e.mail", Password: "secret"},
			codes.OK,
		},
		{
			"test create exist account",
			&pb.AccountData{Email: "exist@e.mail", Password: "secret"},
			codes.AlreadyExists,
		},
		{
			"test internal error",
			&pb.AccountData{Email: "error@e.mail", Password: ""},
			codes.Internal,
		},
	}
 
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			request := tt.req
 
			response, err := client.RegisterByEmail(ctx, request)
			if response != nil && tt.errCode != codes.OK{t.Error("code: expected", codes.OK.String(), "received", tt.errCode.String())}
			if err != nil {
				if er, ok := status.FromError(err); ok {
					if er.Code() != tt.errCode {
						t.Error("error code: expected", tt.errCode, "received", er.Code())
					}
					
				}
			}
		})
	}
}

func TestAuthActivateAccount(t *testing.T) {

	ctx := context.Background()
	conn, err := grpc.DialContext(ctx, "", grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithContextDialer(dialer()))
	if err != nil {t.Fatal(err)}
	defer conn.Close()
 
	client := pb.NewAuthServiceClient(conn)
	response, err := client.RegisterByEmail(ctx, &pb.AccountData{Email: "test@e.mail", Password: "secret"})
	if err != nil {t.Fatal(err)}


	tests := []struct {
		name    string
		req *pb.SingleToken
		errCode codes.Code
	}{
		{
			"test activate account",
			response,
			codes.OK,
		},
		{
			"test activate account with wrong token",
			&pb.SingleToken{Token: response.Token[:len(response.Token)-1]},
			codes.Canceled,
		},
	}
 
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			request := tt.req
 
			response, err := client.Activate(ctx, request)
			if response != nil && tt.errCode != codes.OK{t.Error("code: expected", codes.OK.String(), "received", tt.errCode.String())}
			if err != nil {
				if er, ok := status.FromError(err); ok {
					if er.Code() != tt.errCode {
						t.Error("error code: expected", tt.errCode, "received", er.Code())
					}
					
				}
			}
		})
	}
}

func TestAuthAuth(t *testing.T) {
	ctx := context.Background()
	conn, err := grpc.DialContext(ctx, "", grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithContextDialer(dialer()))
	if err != nil {t.Fatal(err)}
	defer conn.Close()
 
	client := pb.NewAuthServiceClient(conn)

	tests := []struct {
		name    string
		req *pb.AccountData
		errCode codes.Code
	}{
		{
			"test activate account",
			&pb.AccountData{Email: "test@e.mail", Password: "secret"},
			codes.OK,
		},
		{
			"test activate account with wrong creds",
			&pb.AccountData{Email: "test@e.mail", Password: "wrong"},
			codes.Unauthenticated,
		},
	}
 
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			request := tt.req
 
			response, err := client.Auth(ctx, request)
			if response != nil && tt.errCode != codes.OK{t.Error("code: expected", codes.OK.String(), "received", tt.errCode.String())}
			if err != nil {
				if er, ok := status.FromError(err); ok {
					if er.Code() != tt.errCode {
						t.Error("error code: expected", tt.errCode, "received", er.Code())
					}
					
				}
			}
		})
	}
}

func TestAuthRefresh(t *testing.T) {
	ctx := context.Background()
	conn, err := grpc.DialContext(ctx, "", grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithContextDialer(dialer()))
	if err != nil {t.Fatal(err)}
	defer conn.Close()
 
	client := pb.NewAuthServiceClient(conn)
	creds, err := client.Auth(ctx, &pb.AccountData{Email: "test@e.mail", Password: "secret"})
	if err != nil {t.Fatal(err)}
	resp, err := client.Refresh(ctx, &pb.SingleToken{Token: creds.RefreshToken})
	if err != nil && resp == nil {t.Error("expected no error")}
	resp2, err := client.Refresh(ctx, &pb.SingleToken{Token: creds.AccessToken})
	if resp2 != nil {t.Error("expected error")}
	if err != nil {
		if er, ok := status.FromError(err); ok {
			if er.Code() != codes.Unauthenticated {
				t.Error("error code: expected", codes.Unauthenticated, "received", er.Code())
			}
		}
	} else {
		t.Error("expected error")
	}
}