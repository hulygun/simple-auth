package components_test

import (
	"auth/components"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
)

func TestJWT_Create(t *testing.T) {
	t.Parallel()
	TestJWT := &components.JWT{Secret: []byte("test_secret")}
	data := uuid.New()
	token, _, err := TestJWT.Create(data, components.AccessTokenType)
	require.Empty(t, err)
	require.NotEmpty(t, token)
}

func TestJWT_Check(t *testing.T) {
	t.Parallel()
	TestJWT := &components.JWT{Secret: []byte("test_secret")}
	AnotherTestJWT := &components.JWT{Secret: []byte("another_secret")}
	data := uuid.New()
	token, _, _ := TestJWT.Create(data, components.AccessTokenType)
	claims, err := TestJWT.Check(token, components.AccessTokenType)
	require.Empty(t, err)
	require.Equal(t, claims.User, data)
	claims2, err2 := TestJWT.Check(token, components.RefreshTokenType)
	require.Empty(t, claims2)
	require.NotEmpty(t, err2)
	claims3, err3 := AnotherTestJWT.Check(token, components.AccessTokenType)
	require.Empty(t, claims3)
	require.NotEmpty(t, err3)
}
