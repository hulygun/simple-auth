package components

import (
	pb "auth/grpc/auth"
	"context"
	// "fmt"

	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type AuthService struct {
	JWT *JWT
	Log *log.Logger
	Store DataStore
	pb.UnimplementedAuthServiceServer
}

// Register new accont
func (s *AuthService) RegisterByEmail(ctx context.Context, in *pb.AccountData) (*pb.SingleToken, error) {

	exist, err := s.Store.CheckExist(in.Email)
	if err != nil {return nil, s.HandleError(err)}
	if exist {return nil, status.Error(codes.AlreadyExists, "account already exist")}
	uid, err := s.Store.CreateAccount(in)
	if err != nil {return nil, s.HandleError(err)}
	token, _, err := s.JWT.Create(uid, ActivationTokenType)
	if err != nil {return nil, s.HandleError(err)}
	return &pb.SingleToken{Token: token}, nil
}

// Activate accaunt
func (s *AuthService) Activate(ctx context.Context, in *pb.SingleToken) (*pb.Empty, error) {
	claims,  err := s.JWT.Check(in.Token, ActivationTokenType)
	if err != nil {return nil, status.Error(codes.Canceled, "invalid token")}
	err = s.Store.ActivateAccount(claims.User)
	if err != nil {return nil, s.HandleError(err)}
	return &pb.Empty{}, nil
}


// Auth account  Todo: проверка на is_active
func (s *AuthService) Auth(ctx context.Context, in *pb.AccountData) (*pb.TokenData, error) {
	account, err := s.Store.GetAccountByField(in.Email, "email")
	if err != nil {return nil, s.HandleError(err)}
	check := account.CheckPassword(in.Password)
	if !check {return nil, status.Error(codes.Unauthenticated, "invalid credentials")}
	return s.GenerateTokenData(account.Id)
}

// Refresh access token
func (s *AuthService) Refresh(ctx context.Context, in *pb.SingleToken) (*pb.TokenData, error) {
	claims, err := s.JWT.Check(in.Token, RefreshTokenType)
	if err != nil {return nil, status.Error(codes.Unauthenticated, "invalid credentials")}
	return s.GenerateTokenData(claims.User)
}

// Get account by token
func (s *AuthService) Check(ctx context.Context, in *pb.SingleToken) (*pb.AccountMessage, error) {
	claims, err := s.JWT.Check(in.Token, AccessTokenType)
	if err != nil {return nil, status.Error(codes.Unauthenticated, "invalid credentials")}
	account, err := s.Store.GetAccountByField(claims.User.String(), "id")
	if err != nil {return nil, s.HandleError(err)}
	return &pb.AccountMessage{Id: account.Id.String(), Email: account.Email, IsActive: account.IsActive, Created: account.Created}, nil
}

func (s *AuthService) HandleError(err error) error {

	s.Log.Error(err)
	return status.Error(codes.Internal, "internal error")
}

func (s *AuthService) GenerateTokenData(uid uuid.UUID) (*pb.TokenData, error) {
	accessToken, exp, err := s.JWT.Create(uid, AccessTokenType)
	if err != nil {return nil, s.HandleError(err)}
	refreshToken, _, err := s.JWT.Create(uid, RefreshTokenType)
	if err != nil {return nil, s.HandleError(err)}
	result := &pb.TokenData{
		AccessToken: accessToken,
		RefreshToken: refreshToken,
		ExpirationTime: exp,
	}

	return result, nil
}
