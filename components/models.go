package components

import (
	pb "auth/grpc/auth"
	"database/sql"
	"time"

	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

type AccountModel struct {
	Id       uuid.UUID
	Email    string
	IsActive bool
	Created  int64
	password string
}

// Encript account password
func (a *AccountModel) SetPassword(password string) error {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), 10)
	a.password = string(hashedPassword)
	return err
}

// Check password
func (a *AccountModel) CheckPassword(password string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(a.password), []byte(password))
	return err == nil
}

// Create AccountModel from AccountData
func (AccountModel) CreateFromData(data *pb.AccountData) (*AccountModel, error) {
	account := AccountModel{
		Id: uuid.New(),
		Email: data.Email,
		IsActive: false,
		Created: time.Now().Unix(),
	}
	err := account.SetPassword(data.Password)
	return &account, err
}

type DataStore interface {
	CheckExist(email string)                   (bool, error)
	CreateAccount(data *pb.AccountData)        (uuid.UUID, error)
	GetAccountByField(id string, field string) (*AccountModel, error)
	ActivateAccount(id uuid.UUID)              (error)
}


type SQLStore struct {
	DB *sql.DB
	DataStore
}

// Check exist account by email
func (store *SQLStore) CheckExist(email string) (bool, error) {
	var exist bool
	err := store.DB.QueryRow("SELECT EXISTS(SELECT * FROM accounts WHERE email = $1);", email).Scan(&exist)
	return exist, err
}

// Create account
func (store *SQLStore) CreateAccount(data *pb.AccountData) (uuid.UUID, error) {
	account, _ := (AccountModel).CreateFromData(AccountModel{}, data)
	_, err := store.DB.Exec(
		"INSERT INTO accounts (id, email, created, password, is_active) VALUES ($1, $2, $3, $4, $5)", 
		account.Id, account.Email, account.Created, account.password, account.IsActive,
	)
	return account.Id, err
}

// Get account
func (store *SQLStore) GetAccountByField(id string, field string) (*AccountModel, error) {
    var uid uuid.UUID
	var email, password string
	var created int64
	var is_active bool

    err := store.DB.QueryRow(
		"SELECT id, email, created, password, is_active FROM accounts WHERE " + field + " = $1", 
		id,
	).Scan(&uid, &email, &created, &password, &is_active)

	account := &AccountModel{
		Id: uid,
		Email: email,
		password: password,
		Created: created,
		IsActive: is_active,
	}
	return account, err
}

func (store *SQLStore) ActivateAccount(id uuid.UUID) (error) {
	_, err := store.DB.Exec("UPDATE accounts SET is_active = true WHERE id = $1", id)
	return err
}
