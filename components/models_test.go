package components_test

import (
	"auth/components"
	pb "auth/grpc/auth"
	"testing"
	"os"

	"database/sql"

	_ "github.com/mattn/go-sqlite3"
	"github.com/stretchr/testify/require"
)


func TestAccountModel(t *testing.T) {
	t.Parallel()
	testEmail := "test@e.mail"
	testPassword := "secret"
	wrongPassword := "wrong"
	userData := &pb.AccountData{Email: testEmail, Password: testPassword}
	account, err := (components.AccountModel).CreateFromData(components.AccountModel{}, userData)
	require.Nil(t, err)
	require.True(t, account.CheckPassword(testPassword))
	require.False(t, account.CheckPassword(wrongPassword))
}

func TestSQLStore(t *testing.T) {
	path := "test.db"
	file, _ := os.Create(path)
	file.Close()
	db, err := sql.Open("sqlite3", path)
	require.Nil(t, err)

	testEmail := "test@e.mail"
	testPassword := "secret"

	res, err := db.Exec("CREATE TABLE accounts (id VARCHAR CONSTRAINT accounts_pk PRIMARY KEY, email VARCHAR NOT NULL, created   INTEGER NOT NULL, password  VARCHAR, is_active BOOLEAN DEFAULT false); CREATE UNIQUE INDEX accounts_email_uindex ON accounts (email);")
	require.Nil(t, err)

	require.NotEmpty(t, res)
	
	store := components.SQLStore{DB: db}
	check, err := store.CheckExist(testEmail)
	require.Nil(t, err)
	require.False(t, check)

	userData := &pb.AccountData{Email: testEmail, Password: testPassword}

	uid, err := store.CreateAccount(userData)
	require.Nil(t, err)
	require.NotEmpty(t, uid)

	account, err := store.GetAccountByField(uid.String(), "id")
	require.Nil(t, err)
	require.Equal(t, account.Email, testEmail)
	os.Remove(path)
}
