package components

import (
	"errors"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/google/uuid"
)

type TokenType string

const (
	AccessTokenType     TokenType = "access"
	RefreshTokenType    TokenType = "refresh"
	ActivationTokenType TokenType = "activation"
)


type Claims struct {
	User uuid.UUID
	jwt.StandardClaims 
}

type JWT struct {
	Secret               []byte
	AccessExpiration     int
	RefreshExpiration    int
	ActivationExpiration int
}

// Create jwt token by TokenType
func (j *JWT) Create(uid uuid.UUID , t TokenType) (string, int64, error) {
	var duration int

	switch t {
	case AccessTokenType:
		duration = j.AccessExpiration
	case RefreshTokenType:
		duration = j.RefreshExpiration
	case ActivationTokenType:
		duration = j.ActivationExpiration
	}

	expiration := time.Now().Add(time.Duration(duration) * time.Minute).Unix()
	claims := &Claims{User: uid, StandardClaims: jwt.StandardClaims{
		ExpiresAt: expiration,
		Subject:   string(t),
	}}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenStr, err := token.SignedString(j.Secret)
	return tokenStr, expiration,  err
}

// Check valid token 
func (j *JWT) Check(tokenString string, t TokenType) (*Claims, error) {
	token, err := jwt.ParseWithClaims(tokenString, &Claims{}, func(token *jwt.Token) (interface{}, error) {
		return j.Secret, nil
	})
	if claims, ok := token.Claims.(*Claims); ok && token.Valid && claims.Subject == string(t) {
		return claims, nil
	} else {
		if claims.Subject != string(t) {
			err = errors.New("wrong token type")
		}
		return nil, err
	}
}