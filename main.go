package main

import (
	"auth/components"
	pb "auth/grpc/auth"
	"flag"
	"net"

	"github.com/evalphobia/logrus_sentry"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"gopkg.in/ini.v1"

	"database/sql"
	_ "github.com/mattn/go-sqlite3"
)

func main()  {
	// log.SetFormatter(&log.JSONFormatter{})
	
	log := logrus.New()

	cfgFile := flag.String("cfg", "cfg.ini", "# config file")
	flag.Parse()

	cfg, err := ini.Load(*cfgFile)
	if err != nil {log.WithFields(logrus.Fields{"cfg": *cfgFile}).Fatal("config not found")}
	sentry_dsn := cfg.Section("auth").Key("sentry_dsn").String()

	if sentry_dsn != "" {
		log.WithField("sentry", sentry_dsn).Info("sentry dsn")
		hook, err := logrus_sentry.NewSentryHook(sentry_dsn, []logrus.Level{logrus.PanicLevel, logrus.FatalLevel, logrus.ErrorLevel})
		if err == nil {log.Hooks.Add(hook)}
	}

	level, _ := logrus.ParseLevel(cfg.Section("").Key("log_level").String())
	log.SetLevel(level)

	grpcServer := grpc.NewServer()

	access, _ := cfg.Section("auth").Key("access").Int()
	refresh, _ := cfg.Section("auth").Key("refresh").Int()
	activation, _ := cfg.Section("auth").Key("activation").Int()

	db, err := sql.Open("sqlite3", cfg.Section("auth").Key("database").String())
	if err != nil {log.Error(err)}
	defer db.Close()

	pb.RegisterAuthServiceServer(
		grpcServer,
		&components.AuthService{
			Log: log,
			JWT: &components.JWT{
				Secret: []byte(cfg.Section("auth").Key("secret").String()),
				AccessExpiration: access,
				RefreshExpiration: refresh,
				ActivationExpiration: activation,
			},
			Store: &components.SQLStore{DB: db},
		},
	)
	reflection.Register(grpcServer)
	lis, err := net.Listen("tcp", ":"+cfg.Section("auth").Key("service_port").String())
	if err != nil {log.Fatal(err)}

	grpcServer.Serve(lis)
}
